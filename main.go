package main

import (
	"fmt"
	"log"

	"gitlab.com/jesus.vazquez/fizzbuzz/fizzbuzz"
)

func main() {
	// Initialize Processor
	cp := fizzbuzz.NewChainProcessor(
		// Order matters (Look at the divisor before the content))
		fizzbuzz.GetCheckDivisor(),
		fizzbuzz.GetCheckContent(),
	)

	cases := map[int]string{
		51: "FooBar",
		53: "BarFoo",
		21: "FooQix",
		13: "Foo",
		15: "FooBarBar",
		33: "FooFooFoo",
	}
	for k, v := range cases {
		fmt.Printf("Case %d - Expected %s\n", k, v)
		f := fizzbuzz.NewFizzBuzz(k)
		f, err := cp.Process(f)
		if err != nil {
			log.Fatal(err)
		}
		if f.Result() == v {
			fmt.Printf("\t OK: %s\n", f.Result())
		} else {
			fmt.Printf("\t WRONG: %s\n", f.Result())
		}
	}
}
