package fizzbuzz_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jesus.vazquez/fizzbuzz/fizzbuzz"
)

func TestExamples(t *testing.T) {

	tests := map[string]struct {
		input  int
		output string
	}{
		"Look at the divisor before the content (ex. 51 -> FooBar)": {
			input:  51,
			output: "FooBar",
		},
		"Look at the content in the order it is displayed (ex: 53 -> BarFoo)": {
			input:  53,
			output: "BarFoo",
		},
		"Loot at multiples in this order: Foo, Bar, then Qix (ex: 21 -> FooQix)": {
			input:  21,
			output: "FooQix",
		},
		"13 contains 3 so “Foo” will be returned": {
			input:  13,
			output: "Foo",
		},
		"15 is divisible by 3 and 5 and contain 5 so “FooBarBar” will be returned": {
			input:  15,
			output: "FooBarBar",
		},
		"33 contain 3 two times and is divisible by 3 so “”FooFooFoo” will be returned": {
			input:  33,
			output: "FooFooFoo",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			assert := assert.New(t)

			cp := fizzbuzz.NewChainProcessor(
				// Order matters (Look at the divisor before the content))
				fizzbuzz.GetCheckDivisor(),
				fizzbuzz.GetCheckContent(),
			)

			f := fizzbuzz.NewFizzBuzz(test.input)
			f, err := cp.Process(f)
			assert.NoError(err)
			assert.Equal(test.output, f.Output)
		})
	}
}
