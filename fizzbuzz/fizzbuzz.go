package fizzbuzz

import (
	"strconv"
)

// FizzBuzz hold all the FizzBuzz information around a given input
type FizzBuzz struct {
	Input  int
	Output string
}

// NewFooBar initializes and returns new FooBar instance
func NewFizzBuzz(input int) FizzBuzz {
	return FizzBuzz{
		Input:  input,
		Output: "",
	}
}

// Result ~
func (f FizzBuzz) Result() string {
	if f.Output == "" {
		return strconv.Itoa(f.Input)
	} else {
		return f.Output
	}
}
