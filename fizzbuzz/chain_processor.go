package fizzbuzz

import (
	"fmt"
	"strconv"
)

type FizzBuzzProcessor func(FizzBuzz) (FizzBuzz, error)

type ChainProcessor struct {
	processors []FizzBuzzProcessor
}

func NewChainProcessor(processors ...FizzBuzzProcessor) *ChainProcessor {
	return &ChainProcessor{
		processors: processors,
	}
}

func (l *ChainProcessor) Process(f FizzBuzz) (FizzBuzz, error) {
	for i, processor := range l.processors {
		var err error
		f, err = processor(f)
		if err != nil {
			return FizzBuzz{}, fmt.Errorf("foobarr processor %d process: %w", i, err)
		}
	}
	return f, nil
}

// GetCheckDivisor returns a processor that modifies a FizzBuzz according to the
// following rules:
// - Sets output to Foo if input multiple of 3
// - Sets output to += Bar if input multiple of 5
// - Sets output to += Qix if input multiple of 7
// - These modifications are applied in order
func GetCheckDivisor() FizzBuzzProcessor {
	return func(f FizzBuzz) (FizzBuzz, error) {
		if f.Input%3 == 0 {
			f.Output = "Foo"
		}
		if f.Input%5 == 0 {
			f.Output += "Bar"
		}
		if f.Input%7 == 0 {
			f.Output += "Qix"
		}
		return f, nil
	}
}

// GetCheckContent returns a processor that modifies a FizzBuzz according to the
// following rules:
// - For each digit in FizzBuzz input, if its a 3, append Foo to the output
// - For each digit in FizzBuzz input, if its a 5, append Bar to the output
// - For each digit in FizzBuzz input, if its a 7, append Qix to the output
// - It looks at the digits in order so '53' is represented as 'BarFoo'
func GetCheckContent() FizzBuzzProcessor {
	return func(f FizzBuzz) (FizzBuzz, error) {
		tmp := strconv.Itoa(f.Input)

		for _, r := range tmp {
			switch r {
			case '3':
				f.Output += "Foo"
			case '5':
				f.Output += "Bar"
			case '7':
				f.Output += "Qix"
			}
		}

		return f, nil
	}
}
