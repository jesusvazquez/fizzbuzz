# FizzBuzz

## Problem

- If the number is divisible by 3 or contains a 3, return a string containing Foo
- If the number is divisible by 5 or contains a 5, return a string containing Bar
- If the number is divisible by 7 or contains a 7, return a string containing Quix
- Else, return the number

## Rules

In order to avoid ambiguities:

- Look at the divisor before the content (ex. 51 -> FooBar)
- Look at the content in the order it is displayed (ex: 53 -> BarFoo)
- Loot at multiples in this order: Foo, Bar, then Qix (ex: 21 -> FooQix)
- 13 contains 3 so “Foo” will be returned
- 15 is divisible by 3 and 5 and contain 5 so “FooBarBar” will be returned
- 33 contain 3 two times and is divisible by 3 so “”FooFooFoo” will be returned
